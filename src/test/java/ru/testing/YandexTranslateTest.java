package ru.testing;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.testing.entities.TranslateResponse;
import ru.testing.gateway.YandexTranslateGateway;

public class YandexTranslateTest {
    private static final String TEXT = "Hello World!";
    private static final String TARGET_LANGUAGE = "ru";
    private static final String SOURCE_LANGUAGE = "en";
    private static final String TRANSLATE = "Всем Привет!";
//    private static final String TRANSLATE  = "Привет, мир!";

    @Test
    public void TranslateHelloWorld(){
        YandexTranslateGateway yandexTranslateGateway = new YandexTranslateGateway();
        TranslateResponse translateHello = YandexTranslateGateway.getTranslate(TEXT,TARGET_LANGUAGE,SOURCE_LANGUAGE);
        Assertions.assertEquals(translateHello.translations.size(), 1);
        Assertions.assertEquals(translateHello.translations.get(0).text, TRANSLATE);
    }
}
