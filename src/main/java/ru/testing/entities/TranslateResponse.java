package ru.testing.entities;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class TranslateResponse {
    @SerializedName("translations")
    public List<Translation> translations = null;

}