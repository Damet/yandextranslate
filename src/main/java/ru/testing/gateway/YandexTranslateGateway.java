package ru.testing.gateway;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import ru.testing.entities.TranslateResponse;

@Slf4j
public class YandexTranslateGateway  {
        private static final String URL = "https://translate.api.cloud.yandex.net/translate/v2/translate";
        private static final String IAM_TOKEN = "";
        private static final String FOLDER_ID = "";

        @SneakyThrows
        public static TranslateResponse getTranslate(String text, String targetLanguage, String sourceLanguage) {
            Gson gson = new Gson();
            HttpResponse<String> response = Unirest.post(URL)
                .header("Authorization", IAM_TOKEN)
                .header("Content-Type", "application/json")
                .body("{\r\n" +
                    "\"folderId\": \"" + FOLDER_ID + "\",\r\n" +
                    "\"texts\": [\r\n \"" + text + "\"\r\n ],\r\n" +
                    "\"targetLanguageCode\": \"" + targetLanguage + "\",\r\n" +
                    "\"sourceLanguageCode\": \"" + sourceLanguage + "\"\r\n}")
                .asString();
                String strResponse = response.getBody();
                log.info("response: "+strResponse);
                return gson.fromJson(strResponse, TranslateResponse.class);
        }
}
